require('dotenv-flow').config();
import express from 'express';
import userrouter from './Routes/UserRoute'
import bodyparser from 'body-parser';
const app = express();

app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: true }));
app.use(userrouter);
export default app;