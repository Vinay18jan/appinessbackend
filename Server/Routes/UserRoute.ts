import express from 'express';
import * as UserController from '../Controller/UserController';
let userrouter = express.Router();

userrouter.post('/adduser', UserController.AddUser);

export default userrouter;