import SqlHelper from "../Utility/MySqlhelper";

export const AddUser = (req, res, done: Function) => {
    try {
        let sqlHelper = new SqlHelper();
        var params = {
            'p_email':req.body.email,
            'p_password':req.body.password
        }
        sqlHelper.ExecuteSP('create_user', params, function (error, result) {
            if (error) {
                done(error, undefined);
            }
            else if (result.length > 0) {
                done(undefined, result[0][0]);
            }
        })
    }
    catch (ex) {
        done(ex,undefined)
    }
}