import { Request, Response, NextFunction } from "express";
import * as UserModel from '../Model/UserModel'

export let AddUser = (req: Request, res: Response, next: NextFunction) => {
    try{
        UserModel.AddUser(req,res,function(err, result){
            if(err){
                res.status(501).json({
                    "status": false,
                    "data": '',
                    "message": err.message
                });
                next(err);
            }
            else if (result != undefined) {
                res.status(200).json({
                    "status": true,
                    "data": '',
                    "message": "Created Successfully!!!!"
                });
                next();
            }
            else {
                res.status(200).json({
                    "status": false,
                    "data": "",
                    "message": 'Failed to create!!!'
                });
                next();
            }
        })
    }
    catch(ex){
        next(ex);
    }
}