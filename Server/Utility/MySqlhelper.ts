// import connectionPool from '../middleware/mySql';
import mysql, { queryCallback,escape } from 'mysql';
import { QueryOptions } from 'mysql';
import { isNull } from 'util';

export const connectionPool = mysql.createPool({
    connectionLimit : 100,
    host: process.env.SQL_CONNECTION_HOST_ADDRESS,
    user : process.env.SQL_CONNECTION_USERNAME,
    port : 3306,
    password : process.env.SQL_CONNECTION_PASSWORD,
    database : process.env.SQL_CONNECTION_DATABASE,
    multipleStatements: true
});

enum QueryType {
    StoredProcedure,
    Query
}

export default class SqlHelper {

    public ExecuteSP (spSignature : string, callback : queryCallback) :void ;
    public ExecuteSP (spSignature : string, parameters : string[] ,  callback : queryCallback) :void ;
    public ExecuteSP (spName : string, parameters : object ,  callback : queryCallback) :void ;
    public ExecuteSP (spSignature : string, parametersOrCallback : string[] | object | queryCallback , callback? : queryCallback ) :void {
        if( parametersOrCallback && typeof parametersOrCallback === "function"){
            let queryoptions : any = this.BuildSqlOptions(spSignature,QueryType.StoredProcedure,undefined);
            queryoptions.nestTables = false;
            connectionPool.query(queryoptions,parametersOrCallback);
        }
        else if (typeof parametersOrCallback === "object" && callback){
            let queryoptions : QueryOptions = this.BuildSqlOptions(spSignature,QueryType.StoredProcedure,parametersOrCallback);
            queryoptions.nestTables = false;
            connectionPool.query(queryoptions,callback);
        }
    }
    
    public ExecuteQuery (sqlquery : string, callback : queryCallback) : void;
    public ExecuteQuery (sqlquery : string, parameters : string[], callback : queryCallback) : void;
    public ExecuteQuery (sqlquery : string, parametersOrCallback : string[] | queryCallback, callback? : queryCallback) {
        if( parametersOrCallback && typeof parametersOrCallback === "function"){
            var queryoptions : QueryOptions = this.BuildSqlOptions(sqlquery,QueryType.Query,undefined);
            connectionPool.query(queryoptions,parametersOrCallback);
        }
        else if ( typeof parametersOrCallback === "object" && callback){
            var queryoptions : QueryOptions = this.BuildSqlOptions(sqlquery,QueryType.Query,parametersOrCallback);
            connectionPool.query(queryoptions,callback);
        }
    }

    private BuildSqlOptions(sqlquery : string, queryType : QueryType, parameters? :string[] | object) : QueryOptions{
        var nestTables = undefined
        if(parameters){
            if(Array.isArray(parameters) && parameters.length > 0){
                sqlquery = mysql.format(sqlquery, parameters);
            }
            else if(typeof parameters === "object") {
                let optionsString : string = this.GetSqlParamStringFromObject(parameters)
                if(optionsString.length > 0){
                    sqlquery = sqlquery.concat('(',optionsString,')'); 
                    //console.log(sqlquery);
                }
            }
        }


        if(queryType == QueryType.StoredProcedure){
            sqlquery = 'CALL ' + sqlquery;
            nestTables = true;
        }
        else {
        }
        var queryOptions : QueryOptions = {
            sql : sqlquery,
            nestTables : nestTables
        }
        console.log(sqlquery);
        console.log("");
        return queryOptions;
    }

    public GetSqlParamStringFromObject(parameters :object) : string {
        let keyList : string[] = Object.getOwnPropertyNames(parameters);       
        if(keyList.length > 0){
            let paramString :string = "";
            let paramList :string[] = [];
            keyList.forEach((key)=>{
                if (isNull(parameters[key]) || (parameters[key] === undefined)) {
                    paramList.push(paramString.concat('@', key, " := ", null));
                }
                else if (parameters[key] === '') {
                    paramList.push(paramString.concat('@', key, " :='", (parameters[key]), "'"));
                }
                else if (typeof parameters[key] === 'number' || typeof parameters[key] === 'boolean') {
                    paramList.push(paramString.concat('@', key, " := ", (escape(parameters[key]))));
                }
                else
                    // if(escape(parameters[key]).split("\\") && escape(parameters[key]).split("\\").length > 0)
                    // {
                    //   paramList.push(paramString.concat('@', key, " := ", (escape(parameters[key]).split("\\").join("\\\\\\")), ""));
                    // }
                    // else
                    paramList.push(paramString.concat('@', key, " := ", (escape(parameters[key])), ""));

            })
            paramString = paramList.join();
            return paramString;
        }
        else
            return "";
    }

    public ExecuteBulkInsert(tablename: string, data: any[] | object, callback?: queryCallback, tablecolumns?: any[]) {
        var queryoptions: QueryOptions = this.GenerateMultipleSqlStatments(tablename, data, tablecolumns);
        connectionPool.query(queryoptions, callback);
    }

    private GenerateMultipleSqlStatments(tablename, data: object, tablecolumns?): QueryOptions {

        var columns: any[] = new Array();
        var values: any[] = new Array();
        if (Array.isArray(data) && data.length > 0) 
        {
            var firstObj = data[0]; 
            if (tablecolumns!= undefined && tablecolumns != null && tablecolumns.length > 0) {
                columns = tablecolumns;
            }
            else
                columns = Object.keys(firstObj);// retrieve first object in array

                data.forEach((obj) => values.push(
                Object.values(obj)
                ));
        }

        var sqlquery = sqlquery = mysql.format(" INSERT INTO ??(??) VALUES ? ", [tablename, columns, values]);

        var queryOptions: QueryOptions = {
            sql: sqlquery,
            nestTables: false
        }
        return queryOptions;
    }
}